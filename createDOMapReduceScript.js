/**
 * Module Description
 *	Version    Date            Author           Remarks
 * 2.00       10 Apr 2020     Leela Supraja Kuricheti
 * 2.01       22 Jun 2020 	  Emmanuel Nwaobi	Added handlers for Duplicate NSN creation Error
 * 2.02       09 Feb 2021     Emmanuel Nwaobi   Added error validation for special characters in NSN, DTID, MILSDoc, and SO #
 */
/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 */
define(['N/search','N/record','N/runtime','N/format','N/task','N/error'],

		function(search,record,runtime,format,task,error) {

	/**
	 * Marks the beginning of the Map/Reduce process and generates input data.
	 *
	 * @typedef {Object} ObjectRef
	 * @property {number} id - Internal ID of the record instance
	 * @property {string} type - Record type id
	 *
	 * @return {Array|Object|Search|RecordRef} inputSummary
	 * @since 2015.1
	 */
	function getInputData() {

		var scriptObj = runtime.getCurrentScript();
		log.debug("Deployment Id: " + scriptObj.deploymentId);
		var deploymentid = scriptObj.deploymentId;
		log.debug('deployment ID',deploymentid);

		/*var startIndex = 0;
    	var endIndex = 100;*/
		//startIndex + 1000; 


		var itemSearch = search.create({
			type: 'customrecord_ip_do_upload',
			filters:[
				['custrecord_ip_do_created', 'is', 'F'],
				'AND',
				['custrecord_ip_error_box', 'is', 'F']
				],
				columns:[

					({name: 'internalid'})

					]
		});

		if(deploymentid == 'customdeploy_ip_mr_create_do'){

			var startIndex = 0;
			var endIndex = 100;

			log.debug('startIndex',startIndex);
			log.debug('endIndex',endIndex);

			var itemSearchResults = itemSearch.run().getRange({
				start: startIndex,
				end: endIndex
			});
			log.debug('itemSearchResults',itemSearchResults);
			if(itemSearchResults.length != 0){

				return itemSearchResults;
			}
			else{

				return false

			}

		}
		else
		{
			for(var i=2; i<=8; i++ ){

				//log.debug('i',i);
				log.debug('deploymentid',deploymentid);

				for(var k=2; k<=8; k++){

					//log.debug('k',k);
					var index = deploymentid.search(k);
					log.debug('index',index);

					if(index > 0){

						var startIndex = (100 * (k-1)) + 1; // replace it by 1000
						log.debug('startIndex',startIndex);
						var endIndex = 100 * k;
						log.debug('endIndex',endIndex);

						var itemSearchResults = itemSearch.run().getRange({
							start: startIndex,
							end: endIndex
						});

						log.debug('itemSearchResults',itemSearchResults);
						if(itemSearchResults.length != 0){

							return itemSearchResults;

						}

						else{

							return false

						}

						break;

					}
					else{
						continue;
					}
				}

			}
		}
		return itemSearchResults;		
	}

	/**
	 * Executes when the map entry point is triggered and applies to each key/value pair.
	 *
	 * @param {MapSummary} context - Data collection containing the key/value pairs to process through the map stage
	 * @since 2015.1
	 */
	function map(context) {

		try{
			log.debug('context:',context);

			var itemSearchResults1 = JSON.parse(context.value); //each value is taken
			log.debug('itemSearchResults1',itemSearchResults1);

			var customDoRecord_id = itemSearchResults1.id;//327746;
			log.debug('customDoRecord_id',customDoRecord_id);

			// checking if any DO exists

			var purchaseorderSearchObj = search.create({
				type: "purchaseorder",
				filters:
					[
						["trandate","on","today"], 
						"AND", 
						["mainline","is","T"],
						"AND",
						["custbodydo_upload_recid","is",customDoRecord_id]
						],
						columns:
							[
								({name: "custbodydo_upload_recid"}),
								({name : "internalid"})
								]
			});
			var searchResultCount = purchaseorderSearchObj.runPaged().count;
			log.debug('searchResultCount',searchResultCount);

			if(searchResultCount != 0){

				var DOUploadRecLoad = record.load({
					type : 'customrecord_ip_do_upload',
					id	 :	customDoRecord_id,
					isDynamic: false
				});
				//log.debug('doUpload',doUpload);

				var checkbox = DOUploadRecLoad.getValue({fieldId: 'custrecord_ip_do_created'});
				if(checkbox == false){
					DOUploadRecLoad.setValue({fieldId:'custrecord_ip_do_created', value: true});
					var DOUID = DOUploadRecLoad.save();
					log.debug('DOUID',DOUID);

				}

				var custom_error = error.create({
					name: 'Duplicate Record',
					message: 'delivery order with this DO Upload already exists',
					notifyOff: false
				});
				log.debug("Error Code: " + custom_error.name , custom_error.message);
				throw custom_error;


				/*var results = purchaseorderSearchObj.run();
                      log.debug('results',results);

                      var doUpload_id = record.submitFields({

							type:'customrecord_ip_do_upload',
							id: customDoRecord_id,
							values: {
								custrecord_ip_do_created: true,

						    },
						    options: {
						        enableSourcing: false,
						        ignoreMandatoryFields : true
						    }
						});

						log.debug('doUpload_id', doUpload_id);*/

				return false;	
			}
			//loading DO upload

			var doUpload = record.load({
				type : 'customrecord_ip_do_upload',
				id	 :	customDoRecord_id,
				isDynamic: false
			});
			log.debug('doUpload',doUpload);

			//getting all the values from DO Upload rec to create a DO
			var doByDla = doUpload.getValue('name');
			//log.debug('doByDla',doByDla);
			var salesOrderNum = doUpload.getValue('custrecord_ip_so_num');
			//log.debug('salesOrderNum',salesOrderNum);
			var dtid = doUpload.getValue('custrecord_ip_dtid');
			//log.debug('dtid',dtid);
			var milsdoc = doUpload.getValue('custrecord_ip_milsdoc');
			//log.debug('milsdoc',milsdoc);
			var plant_toConvert = doUpload.getValue('custrecord_ip_plant');
			//log.debug('plant_toConvert',plant_toConvert);

			//convert plant code to upper case
			if(plant_toConvert){

				var plant = plant_toConvert.toUpperCase();
				log.debug('plant',plant);

			}		
			var fsc = doUpload.getValue('custrecord_ip_fsc');
			//log.debug('fsc',fsc);
			var materialbefore = doUpload.getValue('custrecord_ip_material');
			//log.debug('materialbefore',materialbefore);
			var checkifR = materialbefore.charAt(0);
			//log.debug('checkifR',checkifR);

			//replacing 'R' in material with 0j
			if(checkifR == 'R'){
				materialbefore = materialbefore.replace(materialbefore.charAt(0),'0');
				//log.debug('materialbefore', materialbefore);
			}

			//make sure material has 9 charecters

			var lengthofMat = doUpload.getValue('custrecord_ip_material').length;
			//log.debug('lengthofMat', lengthofMat);

			if(lengthofMat<9) //verify if the material length is less than 9
			{
				//leadingZeros(material,9);
				doUpload.setValue({fieldId:'custrecord_ip_material',value:leadingZeros(materialbefore,9)});
				materialbefore = doUpload.getValue('custrecord_ip_material');
				log.debug('materialbefore after adding leading zeros ',materialbefore);
			}

			var material = doUpload.getValue('custrecord_ip_material');
			//log.debug('material',material);
			var matDescription = doUpload.getValue('custrecord_ip_material_description');
			//log.debug('matDescription',matDescription);
			var demil = doUpload.getValue('custrecord_ip_demil');
			//log.debug('demil',demil);
			var uoi = doUpload.getValue('custrecord_ip_uoi');
			//log.debug('uoi',uoi);
			var cr_quantity = doUpload.getValue('custrecord_ip_quantity');
			//log.debug('cr_quantity',cr_quantity);
			var unitPrice = doUpload.getValue('custrecord_ip_orig_unit_price');
			//log.debug('unitPrice',unitPrice);
			var whLocation = doUpload.getValue('custrecord_ip_wh_loc');
			//log.debug('whLocation',whLocation);
			var nsn_itemName = fsc+materialbefore;
			//log.debug('nsn_itemName', nsn_itemName);

			//getting the subsidiary from script parameters
			var scriptObj = runtime.getCurrentScript();
			var do_subsidary = scriptObj.getParameter({name:'custscript3'});

			log.debug('Subsidary', 'Subsidary'+do_subsidary);

			var DOUploadRec = [];

			DOUploadRec.push({'doByDla':doByDla,'salesOrderNum':salesOrderNum,'customDoRecord_id':customDoRecord_id,
				'milsdoc':milsdoc,'dtid':dtid,'plant':plant,'fsc':fsc,'materialbefore':materialbefore,
				'lengthofMat':lengthofMat,'material':material,'matDescription':matDescription,'demil':demil,
				'uoi':uoi,'cr_quantity':cr_quantity,'unitPrice':unitPrice,'whLocation':whLocation,
				'nsn_itemName':nsn_itemName,'do_subsidary':do_subsidary});

			//context.write
			context.write({ key:customDoRecord_id, value:DOUploadRec });

		}
		catch(parseException){

			log.debug('error',parseException.message);
			log.debug('error',parseException.toString());
			log.debug('error',parseException.name);

		}

	}

	/**
	 * Executes when the reduce entry point is triggered and applies to each group.
	 *
	 * @param {ReduceSummary} context - Data collection containing the groups to process through the reduce stage
	 * @since 2015.1
	 */
	function reduce(context) {

		errorlabel: try{

			log.debug('context',context);

			var doUpID = context.key;
			var len = context.values.length;
			log.debug('len:',len);

			var val = context.values;

			if(val){

				for(i=0;i<len;i++){

					try{

						var DOUploadRec = JSON.parse(context.values[i]);

						log.debug('DOUploadRec',DOUploadRec);

						log.debug('in reduce');
						var doByDla 			= 	DOUploadRec[i].doByDla;
						//log.debug('doByDla',doByDla);
						var salesOrderNum 		= 	DOUploadRec[i].salesOrderNum;
						//log.debug('salesOrderNum',salesOrderNum);
						var customDoRecord_id	= 	DOUploadRec[i].customDoRecord_id;
						//log.debug('customDoRecord_id',customDoRecord_id);
						var milsdoc 			=	DOUploadRec[i].milsdoc;
						//log.debug('milsdoc',milsdoc);
						var dtid 				=	DOUploadRec[i].dtid;
						//log.debug('dtid',dtid);
						var plant 				= 	DOUploadRec[i].plant;
						//log.debug('plant',plant);
						var fsc 				=	DOUploadRec[i].fsc;
						//log.debug('fsc',fsc);
						var materialbefore 		=	DOUploadRec[i].materialbefore;
						//log.debug('materialbefore',materialbefore);
						var lengthofMat 		=	DOUploadRec[i].lengthofMat;
						//log.debug('lengthofMat',lengthofMat);
						var material			=	DOUploadRec[i].material;
						//log.debug('material',material);
						var matDescription		=	DOUploadRec[i].matDescription;
						//log.debug('matDescription',matDescription);
						var demil				=	DOUploadRec[i].demil;
						//log.debug('demil',demil);
						var uoi					=	DOUploadRec[i].uoi;
						//log.debug('uoi',uoi);
						var cr_quantity			=	DOUploadRec[i].cr_quantity;
						//log.debug('cr_quantity',cr_quantity);
						var unitPrice			=	DOUploadRec[i].unitPrice;
						//log.debug('unitPrice',unitPrice);
						var whLocation			=	DOUploadRec[i].whLocation;
						//log.debug('whLocation',whLocation);
						var nsn_itemName		=	DOUploadRec[i].nsn_itemName;
						//log.debug('nsn_itemName',nsn_itemName);
						var do_subsidary		=	DOUploadRec[i].do_subsidary;
						log.debug('do_subsidary',do_subsidary);

						//check if DTID, MILSDoc, SO #, or NSN has invalid character(s)
						var errCount = 0;
						var errorMsg = '';

						if(!dtid.match("^[A-Za-z0-9]+$") || !milsdoc.match("^[A-Za-z0-9]+$")) {
							errorMsg += 'Invalid character(s) for \'DTID\' and/or \'MILSDoc\' field\n';
							++errCount;
						}

						if(!salesOrderNum.match("^[A-Za-z0-9]+$")) {
							errorMsg += 'Invalid character(s) for \'Sales Order #\' field\n';
							++errCount;
						}

						if(!fsc.match("^[A-Za-z0-9]+$") || !material.match("^[A-Za-z0-9]+$")) {
							errorMsg += 'Invalid character(s) for \'NSN\' field';
							++errCount;
						}

						if(errCount) {
							throw errorMsg;
							break errorlabel;
						}

						var purchaseorderSearchObj = search.create({
							type: "purchaseorder",
							filters:
								[
									["trandate","on","today"], 
									"AND", 
									["mainline","is","T"],
									"AND",
									["custbodydo_upload_recid","is",customDoRecord_id]
									],
									columns:
										[
											({name: "custbodydo_upload_recid"}),
											({name : "internalid"})
											]
						});
						var searchResultCount = purchaseorderSearchObj.runPaged().count;
						log.debug('searchResultCount',searchResultCount);

						if(searchResultCount != 0){
							//var results = purchaseorderSearchObj.run();
							//log.debug('results',results);

							var custom_error = error.create({
								name: 'Duplicate Record',
								message: 'delivery order with this DO Upload already exists',
								notifyOff: false
							});
							log.debug("Error Code: " + custom_error.name , custom_error.message);
							throw custom_error;

							/*var doUpload_id = record.submitFields({

							type:'customrecord_ip_do_upload',
							id: customDoRecord_id,
							values: {
								custrecord_ip_do_created: true,

						    },
						    options: {
						        enableSourcing: false,
						        ignoreMandatoryFields : true

						    }
						});
						//var  doUpload_id = doUpload.save();
						log.debug('doUpload_id', doUpload_id);
							 */
							return false;	
						}

						//continue;

						//creating delivery order
						var item_id;
						var amount;


						var deliveryOrder = record.create({
							type: 'purchaseorder',
							isDynamic: true
						});

						deliveryOrder.setValue('custbodydo_upload_recid', customDoRecord_id);
						//log.debug('0');

						deliveryOrder.setValue('custbody_ip_dla_do_number', doByDla);
						//log.debug('1');

						if(salesOrderNum){
							deliveryOrder.setValue('custbody_ip_do_sales_order_num',salesOrderNum);
							//log.debug('2');
						}

						if(plant.indexOf('DD') > -1)
						{
							deliveryOrder.setValue('custbody_ip_dtid', milsdoc);
							//log.debug('3');
						}else
						{
							deliveryOrder.setValue('custbody_ip_dtid', dtid);
							//log.debug('4');
						}

						deliveryOrder.setValue('custbody_ip_qty_at_header', cr_quantity);
						//log.debug('5');

						log.debug('plant',plant);

						var searchType = search.Type.LOCATION;
						log.debug('searchType',searchType);
						var operator = search.Operator.CONTAINS;
						log.debug('operator',operator);
						var searchFilter = plant;

						var locationSearchResult = valueSearch(searchType,operator,searchFilter);
						log.debug('locationSearchResult',locationSearchResult);

						if(locationSearchResult)
						{
							//log.debug('7');

							var plant_location = locationSearchResult[0].getValue('name');
							log.debug('Plant Location', plant_location);

							var locationId = locationSearchResult[0].getValue('internalid');
							log.debug('locationId', locationId);

							//set value of entity ie, vendor
							if(plant_location.indexOf('EAST')>-1){

								deliveryOrder.setValue('entity',116);
								//log.debug('8');

							}else if(plant_location.indexOf('WEST')>-1){

								deliveryOrder.setValue('entity',118);
								//log.debug('9');
							}
							var vendor =  deliveryOrder.getValue('entity');
							log.debug('vendor', vendor);

							//if location exists then set the subsidiary
							deliveryOrder.setValue('subsidiary', do_subsidary);
							var dsubsidiary = deliveryOrder.getValue('subsidiary');
							log.debug('dsubsidiary', dsubsidiary);

							var parent_location = plant_location.substring(0,( plant_location.indexOf(' :')));//get value before :
							log.debug('parent_location', parent_location);

							var child_location = plant_location.substring(plant_location.indexOf(':') + 1); //get value after :
							log.debug('child_location', child_location);
						}

						// search for getting the parent location

						var searchType = search.Type.LOCATION;
						log.debug('searchType',searchType);
						var operator = search.Operator.IS;
						log.debug('operator',operator);
						var searchFilter = parent_location;

						var parentLocRes = valueSearch(searchType,operator,searchFilter);
						log.debug('parentLocRes',parentLocRes);

						if(parentLocRes)
						{
							log.debug('parentLocRes', parentLocRes);
							var parent_location_from_search = parentLocRes[0].getValue('name');
							log.debug('parent_location_from_search', parent_location_from_search);
							var parent_locationId = parentLocRes[0].getValue('internalid');
							log.debug('parent locationId', parent_locationId);

							if(plant.indexOf('DD') > -1){
								//log.debug('10');
								deliveryOrder.setValue({fieldId:'location',value: parent_locationId});
							}
							else{
								//log.debug('11');
								deliveryOrder.setValue({fieldId:'location',value: locationId});//correct
							}
						}


						//line level

						deliveryOrder.selectNewLine({sublistId : 'item'}); 
						log.debug('selected new line');

						var searchType = search.Type.ITEM;
						log.debug('searchType',searchType);
						log.debug('operator',operator);
						var searchFilter = nsn_itemName;
						log.debug('searchFilter',searchFilter);

						var itemResults = valueSearch(searchType,operator,searchFilter);
						log.debug('itemResults',itemResults);
						log.debug('itemResults length',itemResults.length);

						if(itemResults.length != 0)
						{
							var item_name = itemResults[0].getValue('name');
							log.debug('item_name', item_name);
							item_id = itemResults[0].getValue('internalid');
							log.debug('item_id', item_id);

							if(nsn_itemName == item_name){
								deliveryOrder.setCurrentSublistValue({ sublistId: 'item',fieldId: 'item',value: item_id });
								//log.debug('12');	
							}

						}
						else if (itemResults.length == 0)
						{	
							try{
								//if item doesn't exists creating the item record
								var itemRecord = record.create({
									type: 'lotnumberedinventoryitem',
									isDynamic: false
								});

								log.debug('itemRecord', itemRecord);
								itemRecord.setValue({fieldId:'itemid',value: nsn_itemName});
								itemRecord.setValue({fieldId:'purchasedescription',value: nsn_itemName});
								var lowerCaseDesc = matDescription.toLowerCase();
								log.debug('lowerCaseDesc', lowerCaseDesc);
								var lowerCaseDescription = lowerCaseDesc.split(" ");
								log.debug('lowerCaseDescription', lowerCaseDescription);
								log.debug('lowerCaseDescription length', lowerCaseDescription.length);
								var conversion;
								for (var cv = 0, conversion = lowerCaseDescription.length; cv < conversion; cv++)
								{	
									try{
										lowerCaseDescription[cv] = lowerCaseDescription[cv][0].toUpperCase() + lowerCaseDescription[cv].substr(1);
										log.debug('lowerCaseDescription', lowerCaseDescription[cv][0]);
									}catch(e){
										log.error({ title:'error in description',details: e.message });
									}
								}

								var convertedDesc = lowerCaseDescription.join(" ");
								log.debug('convertedDesc', convertedDesc);
								itemRecord.setValue({fieldId:'salesdescription',value: convertedDesc});
								itemRecord.setValue({fieldId:'purchasedescription',value: convertedDesc});
								itemRecord.setValue({fieldId:'subsidiary',value:1});
								itemRecord.setValue({fieldId:'taxschedule',value:1});
								itemRecord.setValue({fieldId:'includechildren',value: true});
								itemRecord.setValue({fieldId:'usebins',value: true});
								itemRecord.setValue({fieldId:'custitem_ip_update_nsn_desc',value: true});//to convert the desc case
								item_id = itemRecord.save();
								//item created
								log.debug('itemRecordId', item_id);

								//setting the item value
								deliveryOrder.setCurrentSublistValue({ sublistId:'item',fieldId:'item',value:item_id });
							}catch(e){
								try{
									var err = e.message;
									log.error({ title:'Error in NSN creation',details: e.message});
									if (err.indexOf('Uniqueness') > -1){
										var itemResults = valueSearch(searchType,operator,searchFilter);
										log.debug('itemResults',itemResults);
										log.debug('itemResults length',itemResults.length);
										if(itemResults.length != 0)
										{
											var item_name = itemResults[0].getValue('name');
											log.debug('item_name', item_name);
											item_id = itemResults[0].getValue('internalid');
											log.debug('item_id', item_id);

											if(nsn_itemName == item_name){
												deliveryOrder.setCurrentSublistValue({sublistId: 'item',fieldId: 'item',value: item_id });
											}
										}
									}	
								}catch(e){
									log.error({ title:'Error in NSN creation',details: e.message});
								}
							}
						}

						//log.debug('13');
						if(plant.indexOf('DD') > -1)
						{
							deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_ip_dtid',value: milsdoc});

							//log.debug('14');
							deliveryOrder.setValue({fieldId:'custcol_ip_rcp',value:true});//Set the RCP flag to true if it starts with DD
							//log.debug('15');
						}
						else if(plant.indexOf('DD') == -1)
						{
							deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_ip_dtid',value: dtid});
							//log.debug('16');
						}
						//log.debug('17');

						var lineDtid = deliveryOrder.getCurrentSublistValue({sublistId: 'item',fieldId: 'custcol_ip_dtid'});
						log.debug('lineDtid',lineDtid);

						deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'quantity',value: cr_quantity});
						//log.debug('18');

						var lineQuantity = deliveryOrder.getCurrentSublistValue({sublistId: 'item',fieldId: 'quantity'});
						log.debug('lineQuantity',lineQuantity);
						deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_ip_plant_from_do',value: plant });
						//log.debug('i');
						deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_ip_fsc_from_do',value: fsc});
						//log.debug('ii');
						deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_ip_material_from_do',value: materialbefore});
						//log.debug('iii');
						deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'description',value: matDescription});
						//log.debug('iv')
						deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_ip_demil',value: demil});
						//log.debug('v');
						deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_ip_uoi',value: uoi});
						//log.debug('vi');
						deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_ip_orig_unit_price',value: unitPrice});
						//log.debug('19');

						if(locationSearchResult)
						{
							log.debug('plant_location',plant_location);
							if(plant_location.length == 0){
								var plant_location=locationSearchResult[0].getValue('name');
								log.debug('Plant Location', plant_location);
							}
							if(plant_location.indexOf('EAST')>-1){
								deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'entity',value:116});
							}else if(plant_location.indexOf('WEST')>-1){
								deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'entity',value:118});
							}
							var vendor =  deliveryOrder.getCurrentSublistValue({sublistId:'item',fieldId:'entity'});
							log.debug('vendor', vendor);	

							if(plant_location.indexOf('EAST')>-1){
								//log.debug('20');
								deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'rate',value: (unitPrice*0.0585)});
								//log.debug('21');
								amount = (unitPrice*0.0585)*lineQuantity;
								log.debug('amount',amount);
							}
							else if(plant_location.indexOf('WEST')>-1){
								//log.debug('22');
								deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'rate',value: (unitPrice*0.0402)});	
								//log.debug('23');
								amount = (unitPrice*0.0402)*lineQuantity;
								log.debug('amount',amount);
							}
							//log.debug('23');
							deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_ip_source_location',value: locationId});
							//log.debug('24');
						}
						//log.debug('25');
						deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_ip_wh_location',value: whLocation});
						//log.debug('26');	
						var invDetailSubrecord = deliveryOrder.getCurrentSublistSubrecord({
							sublistId: 'item',
							fieldId: 'inventorydetail'
						});

						log.debug('invDetailSubrecord',invDetailSubrecord);

						invDetailSubrecord.selectNewLine({
							sublistId: 'inventoryassignment'
						});

						//log.debug('27');

						if(plant.indexOf('DD') > -1)
						{
							invDetailSubrecord.setCurrentSublistValue({sublistId:'inventoryassignment',fieldId:'receiptinventorynumber',value: milsdoc});
							//log.debug('29');
						}else if(plant.indexOf('DD') == -1)
						{
							invDetailSubrecord.setCurrentSublistValue({sublistId:'inventoryassignment',fieldId:'receiptinventorynumber',value: dtid});
							//log.debug('31');
						}
						//log.debug('32');
						invDetailSubrecord.setCurrentSublistValue({sublistId:'inventoryassignment',fieldId:'quantity',value:lineQuantity});
						//log.debug('33');
						var quantityy = invDetailSubrecord.getCurrentSublistValue({sublistId:'inventoryassignment',fieldId: 'quantity'});
						log.debug('quantityy', quantityy);
						//log.debug('34');
						invDetailSubrecord.commitLine({sublistId:'inventoryassignment'}); 

						log.debug('amount', amount);
						//call the function to calculate the disposition
						var dispositionApproach = calculateDisposition(item_id,amount,fsc);
						log.debug('dispositionApproach', dispositionApproach);
						if(dispositionApproach)
						{
							//set the disposition of the DTID 
							deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_ip_disposition_approach',value:dispositionApproach});
						}else{
							deliveryOrder.setCurrentSublistValue({sublistId:'item',fieldId:'custcol_ip_disposition_approach',value:5});
						}
						deliveryOrder.commitLine('item');

						var today = new Date();
						log.debug('today',today);

						var date1 = format.format({value:today, type: format.Type.DATE});
						log.debug('date1',date1);

						var currentDate = format.parse({value: date1, type: format.Type.DATE});
						log.debug('currentDate',currentDate);

						deliveryOrder.setValue('custbody_ip_disp_updated_on',currentDate);

						var searchResultCount = purchaseorderSearchObj.runPaged().count;
						log.debug('searchResultCount',searchResultCount);

						if(searchResultCount != 0){
							var results = purchaseorderSearchObj.run();
							log.debug('results',results);

							var custom_error = error.create({
								name: 'Duplicate Record',
								message: 'delivery order with this DO Upload already exists',
								notifyOff: false
							});
							log.debug("Error Code: " + custom_error.name , custom_error.message);
							throw custom_error;

							return false;	
						}



						var DOUploadRecLoad = record.load({
							type : 'customrecord_ip_do_upload',
							id	 :	customDoRecord_id,
							isDynamic: false	
						});
						log.debug('DOUploadRecLoad',DOUploadRecLoad);
						var checkbox = DOUploadRecLoad.getValue({fieldId: 'custrecord_ip_do_created'});
						if(checkbox == false){
							DOUploadRecLoad.setValue({fieldId:'custrecord_ip_do_created', value: true});
							var DOUID = DOUploadRecLoad.save();
							log.debug('DOUID',DOUID);
						}



						if(DOUID)
							//saving the record after checking the check box

							var po_id = deliveryOrder.save(); 
						log.debug('Creadted Purchase Order Id Is:', +po_id);



					}
					catch(parseException){

						log.debug('error',parseException.message);
						log.debug('error',parseException.toString());
						log.debug('error',parseException.name);
						var errorMessage;
						if ( parseException instanceof nlobjError ){
							errorMessage= parseException.getDetails();


							var DOUploadRecLoad = record.load({
								type : 'customrecord_ip_do_upload',
								id	 :	customDoRecord_id,
								isDynamic: false	
							});
							log.debug('DOUploadRecLoad',DOUploadRecLoad);

							DOUploadRecLoad.setValue({fieldId:'custrecord_ip_error', value: errorMessage});
							DOUploadRecLoad.setValue({fieldId:'custrecord_ip_error_box', value: true});

							var DOUID = DOUploadRecLoad.save();
							log.debug('DOUID',DOUID);
						}
						else
						{

							errorMessage= parseException.toString();

							var DOUploadRecLoad = record.load({
								type : 'customrecord_ip_do_upload',
								id	 :	customDoRecord_id,
								isDynamic: false	
							});
							log.debug('DOUploadRecLoad',DOUploadRecLoad);

							DOUploadRecLoad.setValue({fieldId:'custrecord_ip_error', value: errorMessage});
							DOUploadRecLoad.setValue({fieldId:'custrecord_ip_error_box', value: true});

							var DOUID = DOUploadRecLoad.save();
							log.debug('DOUID',DOUID);
						}

					}//catch

				}//for

			}//if val

		}//main try
		catch(parseException){
	
			log.debug('error',parseException.message);
			log.debug('error',parseException.toString());
			log.debug('error',parseException.name);
		}

	}


	/**
	 * Executes when the summarize entry point is triggered and applies to the result set.
	 *
	 * @param {Summary} summary - Holds statistics regarding the execution of a map/reduce script
	 * @since 2015.1
	 */
	function summarize(summary) {
		var res = 0;
		var mapSummary = summary.mapSummary;
		var counter = 0;

		mapSummary.keys.iterator().each(function(key){
			counter++;
			return true;
		});




		//	var itemSearchResultsCount = itemSearch.runPaged().count;

		if(counter > 0 && counter <= 100){

			//if(itemSearchResultsCount > 0){

			log.debug('number of records updated ',counter);

			res++;
			log.debug('rescheduled',res+','+'times rescheduled');
			var mapReduceScriptTask = task.create({taskType: task.TaskType.MAP_REDUCE});

			mapReduceScriptTask.scriptId =  'customscript_ip_mr_create_do';//'customscript_test_script';

			//	mapReduceScriptTask.deploymentId = 'customdeploy_ip_mr_create_do'; //'customdeploy_test_script';

			mapReduceScriptTask.submit();
			//}
		}

		else{

			var itemSearch = search.create({

				type: 'customrecord_ip_do_upload',
				filters:[
					['custrecord_ip_do_created', 'is', 'F'],
					'AND',
					['custrecord_ip_error_box', 'is', 'F']
					],
					columns:[

						({name: 'internalid'})

						]

			});

			var itemSearchResultsCount = itemSearch.runPaged().count;
			log.debug('itemSearchResultsCount',itemSearchResultsCount);
			if(itemSearchResultsCount >0 && itemSearchResultsCount < 100 ){

				var mapReduceScriptTask = task.create({taskType: task.TaskType.MAP_REDUCE});

				mapReduceScriptTask.scriptId =  'customscript_ip_mr_create_do';

				mapReduceScriptTask.deploymentId = 'customdeploy_ip_mr_create_do';

				mapReduceScriptTask.submit();

			}

			else{

				log.debug('All the records are processed successfully');

			}

		} 



	}

	function leadingZeros(num, size) {
		try{
			var s = "000000000" + num;
			var matvalue = s.substr(s.length-size);
			//nlapiLogExecution('debug', 'matvalue', matvalue);
			log.debug('matvalue',matvalue);
			return matvalue;
		}
		catch(e){
			log.error('error in function', e); 
		}

	}

	function valueSearch(searchType,operator,searchFilter){

		try{

			log.debug('search type in valueSearch function',searchType);
			log.debug('search filter',searchFilter);
			log.debug('search operator',operator);

			var reqValueSearch = search.create({
				type: searchType,
				filters:[
					['name',operator,searchFilter]
					],
					columns:[

						({name : 'name'}),
						({name : 'internalid'})
						]
			});
			log.debug('reqValueSearch',reqValueSearch);
			var SearchResult = reqValueSearch.run().getRange({
				start: 0,
				end: 1
			});
			log.debug('SearchResult',SearchResult);
			return SearchResult;
		}
		catch(e){

			log.error('error in function valueSearch', e);

		}
	}

	function calculateDisposition(item_id,amount,fsc)
	{
		//data required to calculate the disposition is DTID, NSN,amount
		var disposition;
		var nsnAcqCostSearch = search.create({
			type:'customrecord_ip_nsn_acq_cost_table',
			filter:[
				['custrecord_ip_ac_nsn', 'is',item_id],
				'and',
				['custrecord_ip_nsn_oac_low','lessthanorequalto',amount],
				'and',
				['custrecord_ip_nsn_oac_high','greaterthanorequalto',amount]
				],
				columns:[
					({name: 'custrecord_ip_nsn_disposition'})
					]
		});

		var nsnAcqCostResults = nsnAcqCostSearch.run();
		log.debug('nsn nsnAcqCostResults', JSON.stringify(nsnAcqCostResults));
		if(nsnAcqCostResults){
			if(nsnAcqCostResults.length == 1){
				disposition=nsnAcqCostResults[0].getValue(columns[0]);
				log.debug('nsn disposition', disposition);
				return disposition;
			}
		}else
		{
			log.debug('fsc lookup');

			var fscAcqCostSearch = search.create({
				type:'customrecord_ip_fsc_acq_cost_table',
				filters:[
					['custrecord_ip_ac_fsc', 'is',fsc],
					'and',
					['custrecord_ip_fsc_oac_low','lessthanorequalto',amount],
					'and',
					['custrecord_ip_fsc_oac_high','greaterthanorequalto',amount]
					],
					columns:
						[
							({name: 'custrecord_ip_fsc_disposition'})
							]
			});

			var fscAcqCostResults = fscAcqCostSearch.run();
			log.debug('fscAcqCostResults', JSON.stringify(fscAcqCostResults));
			if(fscAcqCostResults){
				if(fscAcqCostResults.length == 1){
					disposition=fscAcqCostResults[0].getValue(fsccolumn[0]);
					log.debug('fsc disposition', disposition);
					return disposition;			
				}
			}
		}
		//return 0;
	}

	return {
		getInputData: getInputData,
		map: map,
		reduce: reduce,
		summarize: summarize
	};

});
